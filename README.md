<div align="center">
  <img src=".gitlab/logo.svg" height="124px"/><br/>
  <h1> Swipio deploty </h1>
  <p>Simple backend deploy of the <a href="https://gitlab.com/swipio">swipio project</a></p>
</div>

## 📝 About The Project
his project provides a docker-compose file that can be used to quickly and easily deploy the application.

## ⚡️ Quick start

First of all, make sure you have installed **Docker** and **Docker-compose**.

Download the repository and change the current directory:
```bash
$ git clone https://gitlab.com/swipio/deploy.git && cd deploy
```

Deploy backend of the application:
```bash
$ docker-compose up
```

Open the website `http://api.localhost/docs` or `http://fs.localhost/docs` to view the API documentation

## :computer: Contributors

<p>

  :mortar_board: <i>All participants in this project are undergraduate students in the <a href="https://apply.innopolis.university/en/bachelor/">Department of Computer Science</a> <b>@</b> <a href="https://innopolis.university/">Innopolis University</a></i> <br> <br>

  :boy: <b>Gleb Osotov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>g.osotov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitHub: <a href="https://github.com/glebosotov">@glebosotov</a> <br>
  :boy: <b>Vladimir Markov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>v.markov@innopolis.university</a> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; GitLab: <a href="https://gitlab.com/markovvn1">@markovvn1</a> <br>
  :boy: <b>Igor Belov</b> <br>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Email: <a>i.belov@innopolis.university</a> <br>
</p>

## :scroll: License

`Swipio backend` is free and open-source software licensed under the [Apache 2.0 License](LICENSE)

